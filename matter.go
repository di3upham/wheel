package wheel

import "time"

// public model
// TODO use head

// PK(id)
type Merchant struct {
	Id     int    `json:"id"`
	Name   string `json:"name"`
	Status string `json:"status"`
}

// TODO rm depend on db-json, can't rename json tag
// PK(domain)
type Store struct {
	Domain            string   `json:"domain"`
	Title             string   `json:"title"`
	PolicyId          int      `json:"policy_id"`
	GaId              string   `json:"ga_id"`
	TemplatePage      string   `json:"template_page"`
	TypeSellpage      bool     `json:"type_sellpage"`
	Facebook          string   `json:"facebook"`
	Twitter           string   `json:"twitter"`
	Instagram         string   `json:"instagram"`
	Logo              string   `json:"logo"`
	Phone             string   `json:"phone"`
	Email             string   `json:"email"`
	Address           string   `json:"address"`
	StoreName         string   `json:"store_name"`
	AnnoucementBar    string   `json:"annoucement_bar"`
	IsMenu            bool     `json:"is_menu"`
	IsCountdownTime   bool     `json:"is_countdown_time"`
	IsVisitingPeople  bool     `json:"is_visiting_people"`
	IsPurchased       bool     `json:"is_purchased"`
	TopDescription    string   `json:"top_description"`
	MiddleDescription string   `json:"middle_description"`
	BottomDescription string   `json:"bottom_description"`
	PrimaryColor      string   `json:"primary_color"`
	SecondaryColor    string   `json:"secondary_color"`
	TextColor         string   `json:"text_color"`
	BackgroundColor   string   `json:"background_color"`
	Favicon           string   `json:"favicon"`
	LogoWidth         int      `json:"logo_width"`
	Images            []*Image `json:"images"`
}

// Usually, merchant has one shop use for similar onepages, operated by team
// PK(merchant_id,id)
type Shop struct {
	MerchantId     int             `json:"merchant_id"`
	Id             int             `json:"id"`
	Email          string          `json:"email"` // suport email
	PolicyId       int             `json:"policy_id"`
	PaymentGateway *PaymentGateway `json:"payment_gateway"`
	Onepage        *OnepageConfig  `json:"-"`
	FacebookPage   *FacebookPage   `json:"-"` // derived from onepage
	Freshdesk      *Freshdesk      `json:"-"` // derived from onepage
}

// TODO rm depend on db-json, can't rename json tag
type PaymentGateway struct {
	CardPaymentId   int `json:"card_payment_id"`
	PaypalPaymentId int `json:"paypal_payment_id"`
}

// TODO rm depend on db-json, can't rename json tag
// TODO migrate move fb, fd config to shop
// TODO migrate merge remain attrs to onepage
type OnepageConfig struct {
	AllowCreditCardPayment bool          `json:"allow_credit_card_payment"`
	LimitOnlyUsCustomer    bool          `json:"limit_only_us_customer"`
	EnablePaypal           bool          `json:"enable_paypal"`
	CheckoutPage           string        `json:"checkout_page"`
	ConfirmShipping        bool          `json:"confirm_shipping"`
	ConfirmBilling         bool          `json:"confirm_billing"`
	DiscountBox            bool          `json:"discount_box"`
	PaypalCreditButton     bool          `json:"paypal_credit_button"`
	Phone                  string        `json:"phone"`
	Address                string        `json:"address"`
	GooglePlacesApiKey     string        `json:"google_places_api_key"`
	FacebookPage           *FacebookPage `json:"fb_page_config"`
	Freshdesk              *Freshdesk    `json:"freshdesk_config"`
}

// TODO rm depend on db-json, can't rename json tag
type FacebookPage struct {
	FbPageId  string `json:"fb_page_id"`
	AllowAll  bool   `json:"allow_all"`
	Index     bool   `json:"index"`
	Static    bool   `json:"static"`
	Trackings bool   `json:"trackings"`
	Contact   bool   `json:"contact"`
	Checkout  bool   `json:"checkout"`
	Thankyou  bool   `json:"thankyou"`
}

// TODO rm depend on db-json, can't rename json tag
type Freshdesk struct {
	FreshdeskEnable   bool   `json:"freshdesk_enable"`
	FreshchatEnable   bool   `json:"freshchat_enable"`
	FreshchatCode     string `json:"freshchat_code"`
	FreshdeshApiUrl   string `json:"freshdesk_url"`
	FreshdeshApiToken string `json:"freshdesk_api_token"`
}

type Onepage struct {
	Data                   *OnepageBuilding `json:"data"`
	MerchantId             int              `json:"merchant_id"`
	ShopId                 int              `json:"shop_id"`
	Id                     int              `json:"id"`
	ImageUrl               string           `json:"image"`
	Title                  string           `json:"title"`
	Url                    string           `json:"url"`
	DefaultPrice           float64          `json:"default_price"`
	ComparedPrice          float64          `json:"compared_price"`
	PolicyId               int              `json:"policy_id"`
	DiscountName           string           `json:"discount_name"`
	Discount               int              `json:"discount"`
	Status                 string           `json:"status"`
	CheckoutPage           string           `json:"checkout_page"`
	Config                 *OnepageConfig   `json:"-"`
	SellessFeePercentage   float64          `json:"percentage_selless_fee"`
	SellessFreeFixedAmount float64          `json:"fixed_amount_selless_fee"`
	Email                  string           `json:"email"`
	PaidOrderCount         int              `json:"paid_order_count"` // derived
}

// TODO rm depend on db-json, can't rename json tag
type OnepageBuilding struct {
	Favicon   string                 `json:"favicon"`
	Shipping  string                 `json:"shipping"`
	Taxes     string                 `json:"taxes"`
	Returns   string                 `json:"returns"`
	Feedbacks []*Feedback            `json:"feedbacks_json"`
	Color     *OnepageColor          `json:"color_config"`
	Product   *Product               `json:"product_json"`
	Config    *OnepageBuildingConfig `json:"config_json"`
	Prodenv   *Prodenv               `json:"production_env"`
}

// TODO rm
type Prodenv struct {
	PaypalClientId string `json:"VUE_APP_PAYPAL_CLIENT_ID"`
	AppDescription string `json:"VUE_APP_DESCRIPTION"`
	AppImage       string `json:"VUE_APP_IMAGE"`
	AppTitle       string `json:"VUE_APP_TITLE"`
	AppUrl         string `json:"VUE_APP_URL"`
}

// TODO rm depend on db-json, can't rename json tag
// TODO merge onepage building
type OnepageColor struct {
	Primary    string `json:"primary_color"`
	Secondary  string `json:"secondary_color"`
	Text       string `json:"text_color"`
	Background string `json:"background_color"`
	LogoWidth  string `json:"logo_width"`
}

// TODO rm depend on db-json
type Feedback struct {
	Name   string   `json:"name"`
	Rating float64  `json:"rating"`
	Short  string   `json:"short"`
	Detail string   `json:"detail"`
	Images []*Image `json:"images"`
}

type ProductTemplate struct {
	MerchantId  int      `json:"merchant_id"`
	OnepageId   int      `json:"onepage_id"`
	Id          int      `json:"id"`
	Title       string   `json:"title"`
	CustomTitle string   `json:"custom_title"`
	Images      []*Image `json:"images"`
}

// TODO rm depend on db-json, can't rename json tag
type Product struct {
	Title            string              `json:"title"`
	Description      string              `json:"description"`
	ShortDescription string              `json:"shortDescription"`
	Thumbnail        int                 `json:"thumbnail"` // 0
	Images           []string            `json:"images"`
	Properties       map[string][]string `json:"properties"`
	Variants         []*ProductVariant   `json:"variants"`
	SortedProperties []*KVArr            `json:"sorted_properties"`
}

type KVArr struct {
	Name   string   `json:"name"`
	Values []string `json:"values"`
}

// TODO rm depend on db-json
type Image struct {
	Id  int    `json:"id"`
	Url string `json:"url"`
}

// TODO merge to onepage-building
// TODO rm depend on db-json, can't rename json tag
type OnepageBuildingConfig struct {
	Config                *OnepageConfig `json:"pageConfigs"`
	StickyCart            *StickyCart    `json:"stickyCart"`
	MerchantId            int            `json:"merchant_id"`
	MerchantName          string         `json:"merchant"`
	Domain                string         `json:"myonepageDomain"`
	ShopEmail             string         `json:"shopEmail"`
	OnepageId             int            `json:"onepageId"`
	OnepageTitle          string         `json:"onepageTitle"`
	OnepageName           string         `json:"onepageName"`
	Email                 string         `json:"email"`
	Logo                  string         `json:"logo"`
	HasCart               bool           `json:"hasCart"`
	VariantsDisplay       string         `json:"variantsDisplay"`
	FacebookPixelId       string         `json:"pixelId"`
	TikTokPixelId         string         `json:"tikTokPixelId"`
	GaId                  string         `json:"gaId"`
	GaScript              string         `json:"gaScript"`
	ShowAnnoucementBar    bool           `json:"showAnnoucementBar"`
	AnnoucementBarText    string         `json:"annoucementBarText"`
	FooterDescription     string         `json:"footerDescription"`
	PixelContentId        int            `json:"pixelContentId"`
	HasMenu               bool           `json:"hasMenu"`
	MetaDescription       string         `json:"metaDescription"`
	ShortDescription      string         `json:"shortDescription"`
	TopDescription        string         `json:"topDescription"`
	MiddleDescription     string         `json:"middleDescription"`
	BottomDescription     string         `json:"bottomDescription"`
	CountdownTime         int            `json:"countdownTime"`
	NumVisitingPeople     int            `json:"numVisitingPeople"`
	NumPurchased          int            `json:"numPurchased"`
	Facebook              string         `json:"facebook"`
	ShippingRules         []string       `json:"shippingRules"`
	DefaultPixelIds       []string       `json:"defaultPixelIds"`
	DefaultTikTokPixelIds []string       `json:"defaultTikTokPixelIds"`
	DiscountCode          string         `json:"discountCode"`
	DiscountPercent       int            `json:"discountPercent"`
}

// TODO rm depend on db-json
type StickyCart struct {
	Enable           bool   `json:"enable"`
	BarColor         string `json:"bar_color"`
	BarTextColor     string `json:"bar_text_color"`
	ButtonColor      string `json:"button_color"`
	ButtonTextColor  string `json:"button_text_color"`
	TextButton       string `json:"text_button"`
	AnimationButton  string `json:"animation_button"`
	ActionButton     string `json:"action_button"`
	ShowVariant      bool   `json:"show_variant"`
	ShowImage        bool   `json:"show_image"`
	ShowComparePrice bool   `json:"show_compare_price"`
	ShowPrice        bool   `json:"show_price"`
	ShowQuantity     bool   `json:"show_quantity"`
	ShowTitle        bool   `json:"show_title"`
}

// TODO rm depend on db-json, can't rename json tag
// TODO ProductVariant.Payload unknown json
// OnepageVariant, OnepageProductVariantMapping
// ProductVariant.IsSalable derived
type ProductVariant struct {
	MerchantId                     int                 `json:"merchant_id"`
	Id                             int                 `json:"id"`
	ComparedPrice                  float64             `json:"compared_price"`                     // pv, cpvm, cv
	DefaultPrice                   float64             `json:"default_price"`                      // pv, cpvm, cv
	Image                          string              `json:"image_url"`                          // pv, cpvm, cv
	OnepageVariantId               int                 `json:"onepage_variant_id"`                 // pv, cpvm
	AllowToSell                    bool                `json:"allow_to_sell"`                      // pv, cpvm
	Sku                            string              `json:"sku"`                                // pv, cv
	Usku                           string              `json:"usku"`                               // pv, cv
	Status                         string              `json:"status"`                             // pv, cv
	DefaultPrice0                  float64             `json:"price"`                              // pv
	ComparedPrice0                 float64             `json:"comparedPrice"`                      // pv
	Properties                     map[string]string   `json:"properties"`                         // pv, derived
	PropertiesName                 string              `json:"properties_name"`                    // pv, derived
	PropertiesObj                  []map[string]string `json:"properties_obj"`                     // pv, derived
	ProductTemplate                *ProductTemplate    `json:"product_template"`                   // pv, derived
	OnepageVariant                 *ProductVariant     `json:"onepage_variant"`                    // pv, derived
	OnepageProductVariantMapping   *ProductVariant     `json:"onepage_product_variant_mapping"`    // pv, derived
	ProductCost                    *ProductCost        `json:"product_cost"`                       // pv, derived
	Image0                         int                 `json:"image"`                              // pv
	ProductTemplateId              int                 `json:"product_template_id"`                // pv
	Cost                           float64             `json:"cost"`                               // pv
	IsSale                         bool                `json:"is_sale"`                            // pv
	PriceId                        int                 `json:"price_id"`                           // pv
	OnepageProductCostId           int                 `json:"onepage_product_cost_id"`            // pv
	OnepageProductVariantMappingId int                 `json:"onepage_product_variant_mapping_id"` // pv
	Sequence                       int                 `json:"sequence"`                           // cpvm, cv
	Options                        []*VariantOption    `json:"options"`                            // cpvm, cv
	OnepageProductId               int                 `json:"onepage_product_id"`                 // cpvm
	Inventory                      int                 `json:"inventory"`                          // cv
	ProductId                      int                 `json:"product_id"`                         // cv
	ContractStatus                 string              `json:"contract_status"`                    // cv
	HasContract                    bool                `json:"has_contract"`                       // cv
}

// TODO rm depend on db-json
type VariantOption struct {
	Id       int    `json:"id"`
	Name     string `json:"name"`
	Value    string `json:"value"`
	Sequence int    `json:"sequence"`
}

type Domain struct {
	MerchantId int    `json:"merchant_id"`
	OnepageId  int    `json:"onepage_id"`
	Id         int    `json:"id"`
	Domain     string `json:"domain"`
	Status     string `json:"status"`
	PolicyId   int    `json:"policy_id"`
	IsPrimary  bool   `json:"is_primary"`
	IsDefault  bool   `json:"is_default"`
}

type Discount struct {
	MerchantId int        `json:"merchant_id"`
	Id         int        `json:"id"`
	Status     string     `json:"status"`
	Code       string     `json:"discount_code"`
	Scope      string     `json:"discount_scope"`
	ShopId     int        `json:"shop_id"`
	OnepageIds []int      `json:"onepage_ids"`
	StartAt    *time.Time `json:"start_time"`
	EndAt      *time.Time `json:"end_time"`
	Type       string     `json:"discount_type"`
	Value      float64    `json:"discount_value"`
}

type Gateway struct {
	MerchantId  int      `json:"merchant_id"`
	Id          int      `json:"id"`
	Name        string   `json:"name"`
	Adapter     string   `json:"adapter"`
	Cards       []string `json:"cards"`
	Credentials string   `json:"-"`
	OnepageId   int      `json:"onepage_id"`
	ShopId      int      `json:"shop_id"`
}

type Paypal struct {
	ClientId string `json:"client_id"`
}

// TODO rm depend on db-json arr
type Policy struct {
	Type string `json:"type"`
	Data string `json:"data"`
}

type Order struct {
	Id                  int          `json:"id"`
	Name                string       `json:"name"`
	Status              string       `json:"status"`
	FulfillmentStatus   string       `json:"fulfill"`
	Billing             *Address     `json:"billing"`
	Shipping            *Address     `json:"shipping"`
	MerchantId          int          `json:"mid"`
	ShopId              int          `json:"sid"`
	CustomerId          int          `json:"cid"`
	Amount              float64      `json:"value"`
	TransactionId       int          `json:"txn_id"`
	OnepageId           int          `json:"aid"` // Domain string `json:"domain"`
	Token               string       `json:"token"`
	PaymentToken        string       `json:"pay_token"`
	OrderLines          []*OrderLine `json:"items"`
	Customer            *Customer    `json:"customer"`
	Transaction         *Transaction `json:"txn"`
	ShippingName        string       `json:"shipping_name"`
	ShippingPrice       float64      `json:"shipping_price"`
	DiscountId          int          `json:"code_id"`
	DiscountCode        string       `json:"code"`
	DiscountPrice       float64      `json:"code_price"`
	DiscountPercent     float64      `json:"code_percent"`
	IsAbandonedCheckout bool         `json:"-"`
	AbandonedType       string       `json:"-"`
	AbandonedMailStep   string       `json:"-"`
	AbandonedSmsStep    string       `json:"-"`
	PaidAt              *time.Time   `json:"paid_at"`
	SuspectedFraud      bool         `json:"-"`
	FraudTag            string       `json:"-"`
	FraudScore          int          `json:"-"`
	LandingSite         string       `json:"url"`
	Utm                 string       `json:"utm"`
	ReferLink           string       `json:"ref"`
	PriceSum            float64      `json:"price_sum"` // derived, debug
	CreatedAt           *time.Time   `json:"created_at"`
	UpdatedAt           *time.Time   `json:"updated_at"`
}

type LineItemVariantProperty struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

type OrderLine struct {
	MerchantId          int                        `json:"merchant_id"`
	OrderId             int                        `json:"order_id"`
	Id                  int                        `json:"id"`
	Status              string                     `json:"status"`
	UnitPrice           float64                    `json:"u_price"`
	Quantity            int                        `json:"num"`
	Note                string                     `json:"note"`
	UnitProductCost     float64                    `json:"unit_product_cost"`
	UnitFulfillmentCost float64                    `json:"unit_fulfillment_cost"`
	VariantProperties   string                     `json:"variant_properties"`
	ProductTitle        string                     `json:"product_title"`
	VariantImage        string                     `json:"variant_image"`
	VariantProperties2  []*LineItemVariantProperty `json:"variant_properties2"`
	ProductVariantId    int                        `json:"variant_id"`
	ProductVariant      *ProductVariant            `json:"product_variant"`
	ComparedPrice       float64                    `json:"c_price"`        // derived
	ImageUrl            string                     `json:"img_url"`        // derived
	PropertiesObj       []map[string]string        `json:"properties_obj"` // derived
	CreatedAt           *time.Time                 `json:"created_at"`
	UpdatedAt           *time.Time                 `json:"updated_at"`
}

type TrackingInfo struct {
	TrackingNumber string `json:"tracking_number"`
	TrackingStatus string `json:"tracking_status"`
}

// TODO rm depend on db-json
type Address struct {
	MerchantId   int        `json:"merchant_id"`
	Id           int        `json:"id"`
	CustomerId   int        `json:"customer_id"`
	Address1     string     `json:"address1"`
	City         string     `json:"city"`
	Country      string     `json:"country"`
	PostalCode   string     `json:"postal_code"`
	Zip          string     `json:"zip"`
	Apartment    string     `json:"apartment"`
	Phone        string     `json:"phone"`
	Fullname     string     `json:"fullname"`
	Email        string     `json:"email"`
	Company      string     `json:"company"`
	Address2     string     `json:"address2"`
	Province     string     `json:"province"`
	Longitude    string     `json:"longitude"`
	Latitude     string     `json:"latitude"`
	CountryCode  string     `json:"country_code"`
	ProvinceCode string     `json:"province_code"`
	Shipping     *Address   `json:"shipping"`
	FirstName    string     `json:"first_name"`
	LastName     string     `json:"last_name"`
	CreatedAt    *time.Time `json:"created_at"`
	UpdatedAt    *time.Time `json:"updated_at"`
}

type Customer struct {
	Address       *Address   `json:"address,omitempty"`
	ContactMethod string     `json:"contact_method,omitempty"`
	Gender        string     `json:"gender,omitempty"`
	Status        string     `json:"status,omitempty"`
	Email         string     `json:"email,omitempty"`
	Phone         string     `json:"phone,omitempty"`
	FirstName     string     `json:"first_name,omitempty"`
	LastName      string     `json:"last_name,omitempty"`
	CardInto      string     `json:"card_into,omitempty"`
	MerchantId    int        `json:"merchant_id,omitempty"`
	Id            int        `json:"id,omitempty"`
	Name          string     `json:"name,omitempty"`
	Billing       *Address   `json:"billing,omitempty"`
	CreatedAt     *time.Time `json:"created_at"`
	UpdatedAt     *time.Time `json:"updated_at"`
}

type Transaction struct {
	MerchantId           int        `json:"merchant_id"`
	ShopId               int        `json:"shop_id"`
	Id                   int        `json:"id"`
	IdStr                string     `json:"transaction_id"`
	Amount               float64    `json:"amount"`
	PaymentGatewayId     int        `json:"payment_gateway_id"`
	Method               string     `json:"transaction_method"`
	Gateway              string     `json:"transaction_gateway"`
	GatewayTransactionId string     `json:"gateway_transaction_id"`
	Status               string     `json:"status"`
	CurrencyCode         string     `json:"currency_code"`
	CardInfo             string     `json:"card_info"`
	CardInfoHash         string     `json:"card_info_hash"`
	CreatedAt            *time.Time `json:"created_at"`
	UpdatedAt            *time.Time `json:"updated_at"`
}

type ShippingDetail struct {
	MerchantId     int     `json:"merchant_id"`
	Id             int     `json:"id"`
	ShippingRuleId int     `json:"shipping_rule_id"`
	UnitId         int     `json:"unit_id"`
	UnitName       string  `json:"unit_name"`
	Amount         float64 `json:"amount"`
	Quantity       float64 `json:"quantity"`
	Operator       string  `json:"operator"`
	Status         string  `json:"status"`
}

type ShippingRule struct {
	MerchantId      int               `json:"merchant_id"`
	OnepageId       int               `json:"onepage_id"`
	Id              int               `json:"id"`
	Name            string            `json:"name"`
	Type            string            `json:"type"`
	Status          string            `json:"status"`
	ShippingDetails []*ShippingDetail `json:"shipping_details"`
}

type OrderFinancial struct {
	Subtotal        float64 `json:"subtotal"`
	ShippingPrice   float64 `json:"shipping_price"`
	ShippingName    string  `json:"shipping_name"`
	DiscountPrice   float64 `json:"discount_price"`
	DiscountPercent float64 `json:"discount_percent"`
	DiscountCode    string  `json:"discount_code"`
	TotalAmount     float64 `json:"total_amount"`
	PaymentType     string  `json:"payment_type"`
}

type PaypalOrderReq struct {
	Order     *PaypalOrder `json:"order_data"`
	GatewayId string       `json:"gateway_id"`
	OrderId   string       `json:"order_id"`
}

type PaypalOrderRes struct {
	Data *PaypalOrderData `json:"data"`
}

type PaypalOrderData struct {
	Result *PaypalOrder `json:"result"`
}

type PaypalOrder struct {
	Id            string                 `json:"id"`
	Status        string                 `json:"status"`
	Intent        string                 `json:"intent"`
	PurchaseUnits []*PaypalPurechaseUnit `json:"purchase_units"`
	AppContext    *PaypalAppContext      `json:"application_context"`
	Payer         *PaypalPayer           `json:"payer"`
}

type PaypalPayer struct {
	Name  *PaypalShippingName `json:"name"`
	Phone *PaypalPhone        `json:"phone"`
	Email string              `json:"email_address"`
}

type PaypalPhone struct {
	PhoneNumber *PaypalPhoneNumber `json:"phone_number"`
}

type PaypalPhoneNumber struct {
	NationalNumber string `json:"national_number"`
}

type PaypalPurechaseUnit struct {
	ReferenceId    string             `json:"reference_id"`
	Description    string             `json:"description"`
	Amount         *PaypalOrderAmount `json:"amount"`
	Items          []*PaypalOrderItem `json:"items"`
	SoftDescriptor string             `json:"soft_descriptor"`
	Shipping       *PaypalShipping    `json:"shipping"`
	Payments       *PaypalPayments    `json:"payments"`
}

type PaypalPayments struct {
	Captures []*PaypalCapture `json:"captures"`
}

type PaypalCapture struct {
	Id string `json:"id"`
}

type PaypalAppContext struct {
	BrandName          string               `json:"brand_name"`
	ShippingPreference string               `json:"shipping_preference"`
	PaymentMethod      *PaypalPaymentMethod `json:"payment_method"`
}

type PaypalPaymentMethod struct {
	PayeePreferred string `json:"payee_preferred"`
}

type PaypalOrderAmount struct {
	CurrencyCode string                `json:"currency_code"`
	Value        string                `json:"value"`
	Breakdown    *PaypalOrderBreakdown `json:"breakdown"`
}

type PaypalOrderBreakdown struct {
	ItemTotal *PaypalOrderBreakdownValue `json:"item_total"`
	Shipping  *PaypalOrderBreakdownValue `json:"shipping"`
	Discount  *PaypalOrderBreakdownValue `json:"discount"`
}

type PaypalOrderBreakdownValue struct {
	CurrencyCode string `json:"currency_code"`
	Value        string `json:"value"`
}

type PaypalOrderItem struct {
	Name        string                     `json:"name"`
	Description string                     `json:"description"`
	Sku         string                     `json:"sku"`
	UnitAmount  *PaypalOrderBreakdownValue `json:"unit_amount"`
	Quantity    string                     `json:"quantity"`
}

type PaypalShipping struct {
	Name    *PaypalShippingName    `json:"name"`
	Address *PaypalShippingAddress `json:"address"`
}

type PaypalShippingName struct {
	FullName  string `json:"full_name,omitempty"`
	GivenName string `json:"given_name,omitempty"`
	Surname   string `json:"surname,omitempty"`
}

type PaypalShippingAddress struct {
	PostalCode   string `json:"postal_code"`
	CountryCode  string `json:"country_code"`
	AddressLine1 string `json:"address_line_1"`
	AddressLine2 string `json:"address_line_2"`
	AdminArea1   string `json:"admin_area_1"`
	AdminArea2   string `json:"admin_area_2"`
}

type FreshdeskTicket struct {
	Subject     string `json:"subject"`
	Description string `json:"description"`
	Email       string `json:"email"`
	Name        string `json:"name"`
	Status      int    `json:"status"`
	Priority    int    `json:"priority"`
}

type CreditCard struct {
	CardNumber     string `json:"card_number"`
	ExpirationDate string `json:"expiration_date"`
	Cvv            string `json:"cvv"`
	FirstName      string `json:"first_name"`
	LastName       string `json:"last_name"`
}

type CardHolderInfo struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	ZipCode   string `json:"zip_code"`
}

type PayflowOrderReq struct {
	GatewayId      int                   `json:"gateway_id"`
	Amount         float64               `json:"amount"`
	Description    string                `json:"description"`
	CardInfo       *CreditCard           `json:"card_info"`
	CardHolderInfo *CardHolderInfo       `json:"card_holder_info"`
	Shipping       *Address              `json:"shipping"`
	Customer       *Customer             `json:"customer_info"`
	TransactionId  string                `json:"transaction_id"`
	Metadata       *PayflowOrderMetadata `json:"metadata"`
}

type PayflowOrderMetadata struct {
	OrderName string `json:"order_name"`
	Domain    string `json:"domain"`
}

type PayflowOrderRes struct {
	Data *PayflowOrderResData `json:"data"`
}

type PayflowOrderResData struct {
	ResponseCode         string `json:"response_code"`
	TransactionId        string `json:"transaction_id"`
	GatewayTransactionId string `json:"gateway_transaction_id"`
}

type OrderFinance struct {
	MerchantId           int        `json:"merchant_id"`
	OrderId              int        `json:"order_id"`
	Id                   int        `json:"id"`
	Revenue              float64    `json:"revenue"`
	Hold                 float64    `json:"hold"`
	UnholdDate           *time.Time `json:"unhold_date"`
	ProductCost          float64    `json:"product_cost"`
	FulfillmentCost      float64    `json:"fulfillment_cost"`
	PaymentCost          float64    `json:"payment_cost"`
	SellessCost          float64    `json:"selless_cost"`
	AbandonedConvertCost float64    `json:"abandoned_convert_cost"`
	CreatedAt            *time.Time `json:"created_at"`
	UpdatedAt            *time.Time `json:"updated_at"`
}

type WalletHistory struct {
	MerchantId        int        `json:"merchant_id"`
	ShopId            int        `json:"shop_id"`
	Id                int        `json:"id"`
	BalanceChange     float64    `json:"balance_change"`
	TransactionCode   string     `json:"transaction_code"`
	TransactionType   string     `json:"transaction_type"`
	Message           string     `json:"message"`
	RelatedObjectType string     `json:"related_object_type"`
	RelatedObjectId   int        `json:"related_object_id"`
	CreatedAt         *time.Time `json:"created_at"`
	UpdatedAt         *time.Time `json:"updated_at"`
}

type ShopFee struct {
	MerchantId             int        `json:"merchant_id"`
	ShopId                 int        `json:"shop_id"`
	Id                     int        `json:"id"`
	Status                 string     `json:"status"`
	SellessFeePercentage   float64    `json:"percentage_selless_fee"`
	SellessFreeFixedAmount float64    `json:"fixed_amount_selless_fee"`
	PaymentFeePercentage   float64    `json:"percentage_payment_fee"`
	PaymentFeeFixedAmount  float64    `json:"fixed_amount_payment_fee"`
	HoldRate               float64    `json:"hold_rate"`
	NumberHoldDays         int        `json:"number_hold_days"`
	ApplyFrom              *time.Time `json:"apply_from"`
}

type ProductCost struct {
	MerchantId      int     `json:"merchant_id"`
	Id              int     `json:"id"`
	Cost            float64 `json:"product_cost"`
	FulfillmentCost float64 `json:"fulfillment_cost"`
}

type OnepageInsight struct {
	MerchantId              int        `json:"merchant_id"`
	OnepageId               int        `json:"onepage_id"`
	CreatedHour             *time.Time `json:"time_report"`
	Id                      int        `json:"id"`
	RevenueSum              float64    `json:"revenues"`
	ProductCostSum          float64    `json:"product_cost"`
	FulfillmentCostSum      float64    `json:"fulfillment_cost"`
	PaymentCostSum          float64    `json:"payment_cost"`
	AbandonedConvertCostSum float64    `json:"abandoned_convert_cost"`
	SellessCostSum          float64    `json:"selless_cost"`
	HoldSum                 float64    `json:"hold"`
	UnholdSum               float64    `json:"unhold"`
	SellerTakeSum           float64    `json:"seller_take"`
	OrderCount              int        `json:"number_orders"`
	DateTimeId              int        `json:"datetime_report_id"`
}

type SendMailSendGridReq struct {
	Personalizations []*SendGridPersonalization `json:"personalizations"`
	From             *SendGridEmail             `json:"from"`
	Contents         []*SendGridContent         `json:"content"`
	TemplateId       string                     `json:"template_id"`
}

type SendGridPersonalization struct {
	To                  []*SendGridEmail  `json:"to"`
	Subject             string            `json:"subject"`
	DynamicTemplateData map[string]string `json:"dynamic_template_data"`
}

type SendGridEmail struct {
	Email string `json:"email"`
}

type SendGridContent struct {
	Type  string `json:"type"`
	Value string `json:"value"`
}

type LineItem struct {
	MerchantId          int              `json:"merchant_id"`
	OrderId             int              `json:"order_id"`
	UnitPrice           float64          `json:"unit_price"`
	Id                  int              `json:"id"`
	ProcessingAt        *time.Time       `json:"processing_at"`
	VariantId           int              `json:"variant_id"`
	PlatformCreatedAt   *time.Time       `json:"platform_created_at"`
	Code                string           `json:"code"`
	Status              string           `json:"status"`
	ReasonCreated       string           `json:"reason_created"`
	ShippingAddress     *Address         `json:"shipping_address"`
	Priority            string           `json:"priority"`
	OriginId            int              `json:"origin_id"`
	VariantProperties   []*VariantOption `json:"variant_properties"`
	ProductTitle        string           `json:"product_title"`
	Image               *Image           `json:"image"`
	DeliveryStatus      string           `json:"delivery_status"`
	LastStatusUpdatedAt *time.Time       `json:"last_status_updated_at"`
	CreatedAt           *time.Time       `json:"created_at"`
	UpdatedAt           *time.Time       `json:"updated_at"`
}
