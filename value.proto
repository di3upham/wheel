syntax = "proto3";

option go_package = "gitlab.com/di3upham/wheel";

package wheel;

enum Status {
    ACTIVE = 0;
    DELETED = 1;
    INACTIVE = 2;
}

enum AbilityStatus {
    ENABLE = 0;
    DISABLE = 1;
}

enum VisibilityStatus {
    VISIBLE = 0;
    HIDDEN = 1;
}

enum State {
    WAITING = 0;
    START = 1;
    DONE = 2;
    RUNNING = 3;
    CANCEL = 4;
}

enum Scope {
    ALL = 0;
    SPECIFIC = 1;
    ALL_PUBLISHERS = 2;
    SPECIFIC_PUBLISHERS = 3;
    ALL_CELLAS = 4;
    SPECIFIC_CELLAS = 5;
}

enum CheckoutType {
    DEFAULT = 0;
    ADDRESS = 1;
    REDIRECT = 2;
}

enum FunnelType {
    WITH_CART = 0;
    WITHOUT_CART = 1;
}

enum DisplayType {
    DROPDOWN = 0;
    BUTTON = 1;
}

enum EmailEventType {
    ORDER_CONFIRMATION = 0;
    ORDER_NOTIFICATION = 1;
    CHANGE_BILLING = 2;
    SHIPPING_CONFIRM = 3;
    UPDATE_TRACKING_NUMBER = 4;
    ABADONED_CHECKOUT_MAIL_1 = 5;
    ABADONED_CHECKOUT_MAIL_2 = 6;
    ABADONED_CHECKOUT_MAIL_3 = 7;
    ORDER_CANCEL_LINE_ITEM = 8;
    ORDER_CHANGE_LINE_ITEM= 9;
    ORDER_REPLACE_LINE_ITEM = 10;
    ORDER_UPDATE_SHIPPING_ADDRESS = 11;
    ORDER_REFUND = 12;
    ORDER_PROCESSING = 13;
    ORDER_WARNING_SHIPPING = 14;
    ORDER_FULFILLED = 15;
    CUSTOMER_CONTACT = 16;
    ORDER_PAID = 17;
}

enum DiscountType {
    PERCENTAGE = 0;
    FIXED_AMOUNT = 1;
    FREE_SHIPPING = 2;
}

enum PolicyType {
    PRIVACY_POLICY = 0;
    RETURN_POLICY = 1;
    PAYMENT_METHODS_POLICY = 2;
    TERMS_OF_SERVICE_POLICY = 3;
    ABOUT_US_POLICY = 4;
    SHIPPING_POLICY = 5;
    FAQS_POLICY = 6;
    DMCA_POLICY = 7;
}

enum Priority {
    LOW = 0;
    URGENT = 1;
}

message Search {
    enum Type {
        EMAIL_AND_ORDER_NAME = 0;
        CUSTOMER_NAME = 1;
        TRANSACTION_ID = 2;
        PHONE = 3;
        TRACKING_NUMBER = 4;
    }
}

message Contract {
    enum Status {
        NOT_REQUEST = 0;
        WAITING = 1;
        AVAILABLE = 2;
    }
}

message Page {
    enum Status {
        DRAFT = 0;
        ACTIVE = 1;
        BUILDING = 2;
        PUBLISHED = 3;
        CLOSED = 4;
    }
}

message PageDomain {
    enum Status {
        CONNECTING = 0;
        CONNECTED = 1;
        CONNECT_REFUSED = 2;
        ACTIVE = 3;
        DELETED = 4;
    }
}

message Fulfill {
    enum State {
        UNFULFILL = 0;
        PENDING = 1;
        PROCESSING = 2;
        FULFILLED = 3;
        CANCEL = 4;
        DESTROY = 5;
        PARTIALLY_FULFILLED = 6;
    }
}

message Delivery {
    enum State {
        NEW_ORDER = 0;
        TRACKING_AVAILABLE = 1;
        TRACKING_ONLINE = 2;
        IN_US = 3;
        DELIVERED_GUARANTEE = 4;
        COMPLETED = 5;
        REFUND = 6;
    }
}

message ParcelDelivery {
    enum State {
        PENDING = 0;
        PACKED = 1;
        TRANSIT = 2;
        IN_US = 3;
        DEST_TO_PO = 4;
        DELIVERED = 5;
        REFUNDED = 6;
    }
}

enum StoreType {
    STORE_CONFIG = 0;
    TEMPLATE_PAGE = 1;
    TYPE_SELLPAGE = 2;
}

enum RoleType {
    FINANCIAL = 0;
    PRODUCT = 2;
    AM = 3;
    PROCUREMENT = 4;
    CUSTOMER_SUPPORT = 5;
    SUPPLIER = 6;
}

enum OrderStatus {
    DRAFT = 0;
    PENDING = 1;
    AWAITING_CAPTURE = 2;
    PAID = 3;
    PAYMENT_SUCCESS = 4; // paid for transaction
}

message Abandoned {
    enum State {
        FIRST_EMAIL = 0;
        SECOND_EMAIL = 1;
        LAST_EMAIL = 2;
        CANCEL = 3;
    }
    enum Type {
        EMAIL = 0;
        PHONE = 1;
    }
}

enum TagName {
    CREDIT_CARD_USED = 0;
    SAME_ORDER_LINE = 1;
}

message Payout {
    enum State {
        PENDING = 0;
        SYSTEM_REJECTED = 1;
        PROCESSING = 2;
        ADMIN_REJECTED = 3;
        SUCCESS = 4;
        FAILURE = 5;
    }
}

message History {
    enum OrderActionType {
        ADD_NOTE = 0;
        SEND_KLAVIYO = 1;
        FULFILL_REQUISITION_CREATED = 2;
        CHANGE_SHIPPING_ADDRESS = 3;
        POSTPONE = 4;
        REFUND = 5;
        CANCEL = 6;
        RESUME = 7;
        REVOKE = 8;
        SEND_EMAIL = 9;
        ADD_TRACKING_NUMBER = 10;
        CHANGE_TRACKING_NUMBER = 11;
        TRACKING_NUMBER_UPDATE = 12;
        CHANGE_LINE_ITEM = 13;
        ADD_LINE_ITEM = 14;
        REPLACE_LINE_ITEM = 15;
        CANCEL_PROCESSING_ORDER = 16;
        ASSIGN_CONTRACT = 17;
        CHANGE_CUSTOMER_INFO = 18;
        BILLING_ADDRESS_UPDATED = 19;
        CHANGE_LINE_ITEM_SHIPPING_ADDRESS = 20;
        MARK_PRIORITY_URGENT = 21;
    }

    enum TicketActionType {
        CREATE_TICKET = 0;
        CHANGE_STATUS = 1;
        CHANGE_TYPE = 2;
        PUSH_TO_EMAIL = 3;
        PUSH_TO_FRESHDESK = 4;
    }
}

message Wallet {
    enum Type {
        ORDER = 0;
        ORDER_PAID = 1;
        ORDER_HOLD = 2;
        ORDER_UNHOLD = 3;
        PAYOUT = 4;
        PAYOUT_REJECTED = 5;
    }
}

message TrackingQueue {
    enum State {
        PENDING = 0;
        PROCESSING = 1;
        SUCCESS = 2;
        FAIL = 3;
    }
}

message Ticket {
    enum State {
        OPEN = 0;
        PENDING = 1;
        CLOSE = 2;
    }

    enum Type {
        OTHER = 0;
        UPDATE_SHIPPING_ADDRESS = 1;
        CANCEL_DUPLICATE_ORDER = 2;
        CANCEL_WRONG_PURCHASE = 3;
        CANCEL_CHANGE_MIND = 4;
        CANCEL_OTHER = 5;
        CHANGE_ORDER = 6;
        CHANGE_CONTACT_INFO = 7;
        RECEIVED_WRONG_ITEM = 8;
        ITEM_DOES_NOT_FIT = 9;
        ITEM_NOT_AS_DESCRIBED = 10;
        RETURN_OR_REPLACE_OTHER = 11;
        ORDER_STATUS_QUESTION = 12;
        REPORT_ORDER_LATE = 13;
        REPORT_ORDER_MISSING_OR_LOSE = 14;
        REPORT_PAYMENT_UNAUTHORIZED = 15;
        REPORT_BILLING_ISSUE = 16;
    }
}

enum ShopStatus {
    ADMIN_CONFIRMING = 0;
    ADMIN_REJECTED = 1;
    ADMIN_CONFIRMED = 2;
    ADMIN_BANED = 3;
}
